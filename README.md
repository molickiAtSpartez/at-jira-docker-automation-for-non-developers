# at-jira-docker-automation-for-non-developers
## CONFIGURATION

In order to apps working properly one must provide a valid path to a designated docker-compose file.
To do it right one must change `<string />` tag value in **63 line** of
+ `Run jira docker.app/Contents/document.wflow`
+ `Stop jira docker.app/Contents/document.wflow`
to point directly to docker-compose file (`most likely jira-7-12-0-stack.yml`).

## REQUIREMENTS
Docker with docker-compose installed

## RUNNING
Simply launch finder quick search window and type `Run jira docker` or `Stop jira docker`, then press enter

## HOW IT WORKS

#### Run jira docker
Creates a stack of Jira Software 7.12.0 and PostgreSQL with pre-populated data

#### Stop jira docker
Uses the very same docker-compose file to stop created services and clean environment after all are switched off.

## ACCESSING JIRA
In order to log-in one must use the most secret and complex credentials in the world:

`user: admin`
`password: admin`